var app = require('./app.js');
var io = require('socket.io')(app);

var channelEmit = function (socket, message) {
    socket.forEach(element => {
        io.to(element.SocketId).emit('test', message);
    });
}

var FriendsListEmit = function (socket, friendsList) {
    console.log("Emitting ");
    io.to(socket).emit('GetFriendsList', friendsList);
}

var sampleEmit = function (socket, message) {
    socket.forEach(element => {
        io.to(element.SocketId).emit('ReceiveMessage', message);
    });
}

var storeDataEmit = function (socket, message) {
    socket.forEach(element => {
        io.to(element.SocketId).emit('storeData', message);
    });
}

// 'CheckedRegistredUsers' is not emitted on the server but listning in client

var checkedRegisteredUser = function (socket, data) {
    socket.forEach(element => {
        io.to(element.SocketId).emit('CheckedRegisteredUsers', data);
    });
}

module.exports = {
    io,
    channelEmit,
    storeDataEmit,
    sampleEmit,
    FriendsListEmit,
    checkedRegisteredUser
}


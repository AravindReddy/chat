var sqlQuery = require('./sqlDBQueries.js');
var mongoQuery = require('./mongoDBQueries.js');
var validation = require('./validations.js');
var helper = require('./helper.js');
var socketEmision = require('./socketEmissions.js');

var connectionAction = async function (socket) {

    var result = await helper.getSocket(socket.decoded);
    if (result.length > 0) {
        await helper.deleteUserSocket(socket);
    }
    await helper.insertSocket(socket);

    console.log('Socket connected : ', socket.id);
}

var disconnetionAction = async function (socket) {
    await helper.deleteSocket(socket);
    console.log('Socket Disconnected : ', socket.id);
}

var createNewChannel = async function (socket, obj) {
    console.log('new channel socket : ', socket.id);
    console.log('new channel obj : ', obj);
    var jsonObj = JSON.parse(obj);
    var users = await helper.getUserforChat(jsonObj);
    if (users.length > 1) {
        var channelDoc = helper.createChannelDocument(users, jsonObj);
        var channelresult = await helper.createChannel(channelDoc);
        var userDoc = await helper.createruserDocument(users,channelresult);
        var messageDoc = helper.createMessageDocument(channelresult, users, jsonObj.Message, jsonObj.From);
        var result1 = await helper.createMessage(messageDoc);
        var sender = users.find(item => {
            return item.Primary === jsonObj.To
        });
        var senderId = await helper.getSocket(sender._id);
        var result={};
        result.channelRes = channelresult;
        result.messageDoc = result1;
        // var reciever = users.find(item => {
        //     return item.Primary === jsonObj.From
        // });
        // var recieverId = await helper.getSocket(reciever._id);
        socketEmision.channelEmit(senderId,result);
        socketEmision.storeDataEmit(senderId,result);    

        socket.emit('storeData',result);
    }

}

var usersFriendList = async function (socket, username) {
    // console.log('User :', username);

    var friendsList = await helper.getFriendslist(username);
    console.log(friendsList);
    socketEmision.FriendsListEmit(socket.id, friendsList);
}

// var userCheck = async function(obj){
//     var channerUsers = await helper.userCheck(obj); 
//     return channerUsers;
// }

var messagetoExistingChannel = async function (socket, obj) {
    console.log('existing channel socket : ', socket.id);
    console.log('existing channel obj : ', obj);

    var jsonObj = JSON.parse(obj);

    var users = await helper.userCheck(jsonObj);

    if (users.length > 1) {
        var messageDoc = helper.createMessageDocument(channelDoc, users, jsonObj.Message, jsonObj.From);

        var result1 = await helper.createMessage(messageDoc);

        var sender = users.find(item => {
            return item.Email === jsonObj.To
        });

        var senderId = await helper.getSocket(sender._id);

        // var reciever = users.find(item => {
        //     return item.Primary === jsonObj.From
        // });

        // var recieverId = await helper.getSocket(reciever._id);

        socketEmision.messageEmit(senderId, result1);
    }
}

var checkExistingUsers = async function (socket, arrayOfNumbers) {

    var jsonObj = JSON.parse(arrayOfNumbers);
    console.log(jsonObj);
    var data = await helper.getRegisteredUsers(jsonObj);
    console.log('data',data);
    socket.emit('registeredContacts', data);
}

module.exports =
    {
        connectionAction,
        disconnetionAction,
        checkExistingUsers,
        createNewChannel,
        messagetoExistingChannel,
        usersFriendList
    };
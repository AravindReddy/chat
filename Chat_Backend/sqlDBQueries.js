var sqldb = require('./sqldb.js');

var insertSocket = async function (socket) {

    var sqldbconn = await sqldb();

    return new Promise(async (resolve, reject) => {
        try {
            sqldbconn.query("INSERT INTO Users (UserId,SocketId) VALUES ('" + socket.decoded + "', '" + socket.id + "');", function (err, res, more) {
                if (err) {
                    console.log(err.message + 'more = ' + more);
                    if (!more) {
                        console.log('subscribe to multiple errors query');
                        reject(more);
                    }
                    reject(more);
                }
                resolve(res);
            });
        }
        catch (err) {
            console.log(err);
        }
    });
}

var updateSocket = async function (socket) {
    var sqldbconn = await sqldb();

    return new Promise(async (resolve, reject) => {
        try {

            sqldbconn.query("UPDATE Users SET SocketId = '" + socket.id + "' WHERE UserId = '" + socket.decoded + "';", function (err, res, more) {
                if (err) {
                    console.log(err.message + 'more = ' + more);
                    if (!more) {
                        console.log('subscribe to multiple errors query');
                        reject(more);
                    }
                    reject(err);
                }
                resolve(res);
            });
        }
        catch (err) {
            console.log(err);
        }
    });
}

var removeSocket = async function (socket) {

    var sqldbconn = await sqldb();

    return new Promise(async (resolve, reject) => {
        try {
            sqldbconn.query("DELETE FROM Users WHERE SocketId = '" + socket.id + "';", function (err, res, more) {
                if (err) {
                    console.log(err.message + 'more = ' + more);
                    if (!more) {
                        console.log('subscribe to multiple errors query');
                        reject(more);
                    }
                    reject(err);
                }
                resolve(res);
            });
        }
        catch (err) {
            console.log(err);
        }
    });
}

var removeUserSocket = async function (socket) {

    var sqldbconn = await sqldb();

    return new Promise(async (resolve, reject) => {
        try {
            sqldbconn.query("DELETE FROM Users WHERE UserId = '" + socket.decoded + "';", function (err, res, more) {
                if (err) {
                    console.log(err.message + 'more = ' + more);
                    if (!more) {
                        console.log('subscribe to multiple errors query');
                        reject(more);
                    }
                    reject(err);
                }
                resolve(res);
            });
        }
        catch (err) {
            console.log(err);
        }
    });
}

var getSocket = async function (userId) {
    var sqldbconn = await sqldb();
    return new Promise(async (resolve, reject) => {
        try {
            sqldbconn.query("SELECT * from [dbo].[Users] WHERE UserId = '" + userId + "';", function (err, res, more) {
                if (err) {
                    console.log(err.message + 'more = ' + more);
                    if (!more) {
                        console.log('subscribe to multiple errors query');
                        reject(more);
                    }
                    reject(err);
                }
                resolve(res);
            });
        }
        catch (err) {
            console.log(err);
        }
    });
}

module.exports =
    {
        insertSocket,
        removeSocket,
        getSocket,
        updateSocket,
        removeUserSocket
    };

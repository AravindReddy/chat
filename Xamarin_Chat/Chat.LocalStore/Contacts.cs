﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.LocalStore
{
    public class Contacts
    {
        [PrimaryKey][Unique]
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public bool IsRegistered { get; set; }
    }
}

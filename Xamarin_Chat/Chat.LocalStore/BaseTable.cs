﻿using System;

namespace Chat.LocalStore
{
    public class BaseTable
    {
        public string ID { get; set; }
        public bool IsSynced { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}

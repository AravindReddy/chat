﻿using ChatUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatUI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        Task<IList<Plugin.ContactService.Shared.Contact>> ContactsList { get; set; }
        public Login()
        {
            InitializeComponent();
        }

        private async Task ConnectToSocket()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            HttpClient client = new HttpClient();
            dict.Add("primary", entryUsername.Text);
            dict.Add("password", "Welcome2ggk");
            

            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ("http://10.223.0.57:3000" + "/api/auth/login")) { Content = new FormUrlEncodedContent(dict) };
            HttpResponseMessage response = await client.SendAsync(req);
            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
                TokenModel userValues = JsonConvert.DeserializeObject<TokenModel>(res);
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                JwtSecurityToken JST = handler.ReadToken((userValues.token.ToString())) as JwtSecurityToken;
                JwtPayload masterID = JST.Payload;
               object user = masterID["user"];

                App.Current.Properties["Access_Token"] = userValues.token;
                App.Current.Properties["Refresh_Token"] = userValues.refreshToken;
                App.Current.Properties["ExpiryInMinutes"] = userValues.ExpiresIn;
                App.Current.Properties["From"] = entryUsername.Text;

                

                await Navigation.PushAsync(new ChatDetails());
            }
        }

        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            await ConnectToSocket();
        }
    }
}
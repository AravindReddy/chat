using Chat.LocalStore;
using ChatUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace ChatUI
{
	public partial class App : Application
	{
        static DBOperations database;

        public App ()
		{
			InitializeComponent();
            if (App.Current.Properties.ContainsKey("From"))
            {
                MainPage = new NavigationPage(new ChatDetails());
                
            }
            else
            {
                MainPage = new NavigationPage(new Login());
            }
            
            //MainPage = new ChatDetails();
		}

       

        public static DBOperations Database
        {
            get
            {
                if (database == null)
                {
                    database = new DBOperations(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ChatStore.db3"));
                }
                return database;
            }
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

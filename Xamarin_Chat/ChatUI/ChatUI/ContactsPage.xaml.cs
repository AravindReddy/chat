﻿using ChatUI.Models;
using MvvmHelpers;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatUI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactsPage : ContentPage
    {
        public Dictionary<string, ObservableRangeCollection<Message>> MessagesList { get; set; }
        public Socket Socket { get; set; }
        List<Contacts> FriendsList { get; set; }       
        public IList<Plugin.ContactService.Shared.Contact> ContactsList { get; set; }
        public ContactsPage (IList<Plugin.ContactService.Shared.Contact> CurrentContactsList, Socket CurrentSocket,  Dictionary<string, ObservableRangeCollection<Message>> CurrentMessagesList, List<Contacts> CurrentFriendsList)
		{

            InitializeComponent ();
            Socket = CurrentSocket;
            ContactsList = CurrentContactsList;
            MessagesList = CurrentMessagesList;
            FriendsList = CurrentFriendsList;
            lstContacts.BindingContext = ContactsList;

        }

        private void MyItemTapped(object sender, ItemTappedEventArgs e)
        {
            bool flag = false;
           
            ObservableRangeCollection<Message> messages = new ObservableRangeCollection<Message>();
            var content = e.Item as Plugin.ContactService.Shared.Contact;
            //App.Database.CreateTable<Chat.LocalStore.User>();
            List<Chat.LocalStore.User> ChattedUsers = App.Database.Get<Chat.LocalStore.User>();
            foreach(Chat.LocalStore.User friend in ChattedUsers)
            {
                if(friend.PhoneNumber == content.Number)
                {
                    flag = true;
                    List<Chat.LocalStore.Message> PreviousMessages = App.Database.DatabaseObject().Query<Chat.LocalStore.Message>("Select * from [Message] where [Message.ChannelId]=?",friend.ChannelId);
                     
                    foreach(Chat.LocalStore.Message EachMessage in PreviousMessages)
                    {
                        Message Mess = new Message();
                        Mess.Text = EachMessage.MessageText;
                        Mess.MessageDateTime = EachMessage.time;
                        Mess.IsIncoming = EachMessage.IsInComming;
                        messages.Add(Mess);
                    }
                    Navigation.PushAsync(new MainChatPage(content, Socket, messages,flag));
                }
                
            }
            if(flag== false)
            {
                Navigation.PushAsync(new MainChatPage(content, Socket, messages, flag));
                
            }
           
        }

    }
    
}

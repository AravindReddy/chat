﻿using ChatUI.Models;
using MvvmHelpers;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatUI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatDetails : TabbedPage
    {
        public Dictionary<string, ObservableRangeCollection<Models.Message>> MessagesList = new Dictionary<string, ObservableRangeCollection<Models.Message>>();
        IList<Plugin.ContactService.Shared.Contact> ContactsList = new List<Plugin.ContactService.Shared.Contact>();
        private List<Contacts> FriendsList { get; set; }
        public Socket Socket { get; set; }
        public ChatDetails()
        {
            InitializeComponent();

            GetNewRefreshToken();

            IO.Options option = new IO.Options();
            Dictionary<string, string> dictionary = new Dictionary<string, string>
            {
                ["token"] = App.Current.Properties["Access_Token"].ToString()
            };
            option.Query = dictionary;

 

            Socket = IO.Socket("http://10.223.0.57:3000", option);

           

            Socket.On("registeredContacts", (UserStatus) =>
            {
                IList<Plugin.ContactService.Shared.Contact> RegisteredUsers = new List<Plugin.ContactService.Shared.Contact>();

                VerifyNumber[] contactsStatus = JsonConvert.DeserializeObject<VerifyNumber[]>(UserStatus.ToString());

                string ids = string.Empty;

                for (int i = 0; i < contactsStatus.Length; i++)
                {
                    RegisteredUsers.Add(ContactsList.First((contact) => contactsStatus[i].Primary == contact.Number));

                    ids = ids +"'" +contactsStatus[i].Primary +"'"+ ',';
                }

                ids = ids.TrimEnd(',');

                InitializeDatabase();

                App.Database.DatabaseObject().Query<Chat.LocalStore.Contacts>(string.Format("UPDATE Contacts SET IsRegistered = 0 WHERE PhoneNumber in ({0})", ids));

         
                Device.BeginInvokeOnMainThread(() =>
                   {
                       Children.Add(new ChatPage(FriendsList));
                       Children.Add(new ContactsPage(RegisteredUsers, Socket, MessagesList, FriendsList));
                   });
            });

            //Socket.Emit("RequestForFriendsList", App.Current.Properties["From"].ToString());
            //Socket.On("GetFriendsList", (sender) =>
            //{
            //    Device.BeginInvokeOnMainThread(() =>
            //    {
            //        FriendsList = JsonConvert.DeserializeObject<List<Contacts>>(sender.ToString());
            //        // UI 
            //        foreach (var item1 in FriendsList)
            //        {
            //            MessagesList.Add(item1.ChannelId, new ObservableRangeCollection<Models.Message>()); //R
            //        }

            //        Children.Add(new ChatPage(FriendsList));
            //        Children.Add(new ContactsPage(ContactsList, Socket, MessagesList, FriendsList));
            //    });
            //});

            //Socket.On("ReceiveMessage", (sender) => {

            //    GetIncomingMessage(JsonConvert.DeserializeObject<NewChannel>(sender.ToString()));


            //});
        }

        private async Task GetNewRefreshToken()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            HttpClient client = new HttpClient();
            //dict.Add("primary", (string)App.Current.Properties["From"]);
            //dict.Add("password", "Welcome2ggk");
            var ttt = (string)App.Current.Properties["Refresh_Token"];
            dict.Add("refreshToken", (string)App.Current.Properties["Refresh_Token"].ToString());
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ("http://10.223.0.57:3000" + "/api/auth/token")) { Content = new FormUrlEncodedContent(dict) };
            HttpResponseMessage response = await client.SendAsync(req);
            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
                TokenModel userValues = JsonConvert.DeserializeObject<TokenModel>(res);
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                JwtSecurityToken JST = handler.ReadToken((userValues.token.ToString())) as JwtSecurityToken;
                JwtPayload masterID = JST.Payload;
                object user = masterID["user"];

                App.Current.Properties["Access_Token"] = userValues.token;
                App.Current.Properties["Refresh_Token"] = userValues.refreshToken;
                App.Current.Properties["ExpiryInMinutes"] = userValues.ExpiresIn;
            }
        }

        private async void SyncBtnLogin_Clicked(object sender, EventArgs e)
               {
            IList<Plugin.ContactService.Shared.Contact> contacts = Plugin.ContactService.CrossContactService.Current.GetContactListAsync().Result;
            InitializeDatabase();
            List<Chat.LocalStore.Contacts> contactList = App.Database.Get<Chat.LocalStore.Contacts>();

            foreach (Plugin.ContactService.Shared.Contact item in contacts)
            {
                item.Number = Regex.Replace(item.Number, "[^0-9]", "");
                if (item.Number.Length > 10)
                {
                    int start = (item.Number.Length) - 10;
                    string temp = item.Number;
                    item.Number = temp.Substring(2);
                }


                if (!contactList.Any(m => m.PhoneNumber == item.Number))
                {
                    Chat.LocalStore.Contacts con = new Chat.LocalStore.Contacts
                    {
                        Username = item.Name,
                        PhoneNumber = item.Number,
                        IsRegistered = false
                    };

                    App.Database.Save<Chat.LocalStore.Contacts>(con);
                }
            }
            ContactsList = contacts;
            List<VerifyNumber> phoneNumbers = new List<VerifyNumber>();
            for (int i = 0; i < contacts.Count; i++)
            {
                //if()
                //{
                //    Chat.LocalStore.Contacts con = new Chat.LocalStore.Contacts();
                //    con.Username = Contacts[i].Name;
                //    con.PhoneNumber = Contacts[i].Number;
                //    App.Database.Save<Chat.LocalStore.Contacts>(con);
                //}
                phoneNumbers.Add(new VerifyNumber { Primary = contacts[i].Number });
            }
            string json = JsonConvert.SerializeObject(phoneNumbers);
            Socket.Emit("CheckRegisteredUsers", json);

           // return contacts;
        }

        private void InitializeDatabase()
        {
            App.Database.CreateTable<Chat.LocalStore.Contacts>();
            App.Database.CreateTable<Chat.LocalStore.User>();
        }

        //private IList<Plugin.ContactService.Shared.Contact> GetContacts()
        //{
        //    ilist<plugin.contactservice.shared.contact> contacts = plugin.contactservice.crosscontactservice.current.getcontactlistasync().result;
        //    initializedatabase();
        //    list<chat.localstore.contacts> contactlist = app.database.get<chat.localstore.contacts>();

        //    foreach (plugin.contactservice.shared.contact item in contacts)
        //    {
        //        item.number = regex.replace(item.number, "[^0-9]", "");
        //        if (item.number.length > 10)
        //        {
        //            int start = (item.number.length) - 10;
        //            string temp = item.number;
        //            item.number = temp.substring(2);
        //        }


        //        if (!contactlist.any(m => m.phonenumber == item.number))
        //        {
        //            chat.localstore.contacts con = new chat.localstore.contacts
        //            {
        //                username = item.name,
        //                phonenumber = item.number,
        //                isregistered = false
        //            };

        //            app.database.save<chat.localstore.contacts>(con);
        //        }
        //    }

        //    contacts = checkregisteredusers(contacts);
        //    return contacts;
        //}

        //private IList<Plugin.ContactService.Shared.Contact> CheckRegisteredUsers(IList<Plugin.ContactService.Shared.Contact> Contacts)
        //{
        //    List<VerifyNumber> phoneNumbers = new List<VerifyNumber>();
        //    for (int i = 0; i < Contacts.Count; i++)
        //    {
        //        //if()
        //        //{
        //        //    Chat.LocalStore.Contacts con = new Chat.LocalStore.Contacts();
        //        //    con.Username = Contacts[i].Name;
        //        //    con.PhoneNumber = Contacts[i].Number;
        //        //    App.Database.Save<Chat.LocalStore.Contacts>(con);
        //        //}
        //        phoneNumbers.Add(new VerifyNumber { Primary = Contacts[i].Number });
        //    }
        //    string json = JsonConvert.SerializeObject(phoneNumbers);
        //    Socket.Emit("CheckRegisteredUsers", json);

        //    return Contacts;
        //}

        private void GetIncomingMessage(NewChannel EmittedMessage)
        {
            Models.Message messaga = new Models.Message
            {
                Text = EmittedMessage.Message,
                IsIncoming = true,
                MessageDateTime = DateTime.Now
            };
            if (MessagesList.ContainsKey(EmittedMessage.ChannelId))
            {
                MessagesList[EmittedMessage.ChannelId].Add(messaga);
            }
            //else
            //{
            //    ObservableRangeCollection<Message> NewChannelMessage = new ObservableRangeCollection<Message>();
            //    NewChannelMessage.Add(messaga);
            //    Socket.On("ChannelID", (sender) => {
            //        //"Insert into main Page contact list" 
            //    });
            //}

        }
    }
}
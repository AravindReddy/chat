﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatUI
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatPage : ContentPage
	{
        List<Contacts> test { get; set; }
        public ChatPage (List<Contacts> CurrentFriendsList)
		{
			InitializeComponent ();
            test = CurrentFriendsList;
            lstContacts.BindingContext = CurrentFriendsList;
        }
	}
}
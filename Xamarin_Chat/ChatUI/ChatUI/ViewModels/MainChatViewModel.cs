﻿using ChatUI.Models;
using MvvmHelpers;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace ChatUI.ViewModels
{
    public class MainChatViewModel : BaseViewModel
    {

        public Socket Socket { get; set; }
        public string image { get; set; }
        public string name { get; set; }
        public ObservableRangeCollection<Message> Messages { get; set; }
        public bool NewChannelStatus { get; set; }

       // List<Contacts> FriendsList { get; set; }

        private string outgoingText = string.Empty;

        public string OutGoingText
        {
            get => outgoingText;
            set => SetProperty(ref outgoingText, value);
        }

       

        public ICommand SendCommand { get; set; }

        public MainChatViewModel(MainChatPage viewInstance, Plugin.ContactService.Shared.Contact contacts,  Socket SocketInstance, ObservableRangeCollection<Message> CurrentMessagesList,bool flag)
        {
            Messages = CurrentMessagesList;
            name = contacts.Email;
            Socket = SocketInstance;
            NewChannelStatus = flag;
            if (Messages == null)
                Messages = new ObservableRangeCollection<Message>();

            Socket.On("storeData", (data) =>
            {
                StoreData serverdata = JsonConvert.DeserializeObject<StoreData>(data.ToString());

                Chat.LocalStore.Channel channel = new Chat.LocalStore.Channel();
                channel.ChannelType = serverdata.channelRes.ChannelType;
                channel.ChannelCreatedAt = serverdata.channelRes.ChannelCreatedAt;
                channel._id = serverdata.channelRes._id;
                foreach(var item in serverdata.channelRes.ChannelUsers)
                {
                    Chat.LocalStore.ChatUser chatUser = new Chat.LocalStore.ChatUser();
                    chatUser.UserName = item.UserName;
                    channel.ChatUsers.Add(chatUser);
                }
                channel.IsAdmin = false;
                foreach (var item in serverdata.channelRes.ChannelAdmin)
                {
                    if (item.UserName== (String)App.Current.Properties["From"])
                    {
                        channel.IsAdmin = true;
                    }
                }
                App.Database.Save<Chat.LocalStore.Channel>(channel);

                Chat.LocalStore.Message mess = new Chat.LocalStore.Message();
                mess.MessageText = serverdata.messageDoc.MessageText;
                mess.SentByPrimary.UserName = serverdata.messageDoc.Sender.UserName;
                mess.IsInComming = true;
                mess.time = serverdata.messageDoc.MessageCreatedAt;
                //IsSeen
            });
                //Socket.On("ReceiveMessage", (sender) => {

                //    GetIncomingMessage(JsonConvert.DeserializeObject<NewChannel>(sender.ToString()));
                //});

                //Socket.On("storeData", (sender) => {
                //    GetIncomingMessage((NewChannel)sender);
                //});


                //socket.on("storedata", (sender) =>
                //{
                //    getincomingmessage(sender.tostring());
                //});



            SendCommand = new Command(() =>
            {
                Message message = new Message
                {
                    Text = OutGoingText,
                    IsIncoming = false,
                    MessageDateTime = DateTime.Now

                };

                
                    Messages.Add(message);
                
                if (NewChannelStatus == false)
                {
                    NewChannel obj = new NewChannel
                    {
                        From = App.Current.Properties["From"].ToString(),
                        To = contacts.Number,
                        Message = OutGoingText,
                        ChannelId = null
                    };

                    var json = JsonConvert.SerializeObject(obj);
                    Socket.Emit("CreateNewChannel", json);
                }
                else
                {
                    //store in sqlite db
                }


                OutGoingText = string.Empty;
            });
        }

        //private void GetIncomingMessage(NewChannel EmittedMessage)
        //{
        //    Message messaga = new Message
        //    {
        //        Text = EmittedMessage.Message,
        //        IsIncoming = true,
        //        MessageDateTime = DateTime.Now
        //    };
        //    if (ContactList.ContainsKey(EmittedMessage.ChannelId))
        //    {
        //        ContactList[EmittedMessage.ChannelId].Add(messaga);
        //    }
        //    //else
        //    //{
        //    //    ObservableRangeCollection<Message> NewChannelMessage = new ObservableRangeCollection<Message>();
        //    //    NewChannelMessage.Add(messaga);
        //    //    Socket.On("ChannelID", (sender) => {
        //    //        //"Insert into main Page contact list" 
        //    //    });
        //    //}

        //}

      
        public void InitializeMock()
        {
            Messages.ReplaceRange(new List<Message>
                {
                    new Message { Text = "Hi Squirrel! \uD83D\uDE0A", IsIncoming = true, MessageDateTime = DateTime.Now.AddMinutes(-25)},
                    new Message { Text = "Hi Baboon, How are you? \uD83D\uDE0A", IsIncoming = false, MessageDateTime = DateTime.Now.AddMinutes(-24)},
                    new Message { Text = "We've a party at Mandrill's. Would you like to join? We would love to have you there! \uD83D\uDE01", IsIncoming = true, MessageDateTime = DateTime.Now.AddMinutes(-23)},
                    new Message { Text = "You will love it. Don't miss.", IsIncoming = true, MessageDateTime = DateTime.Now.AddMinutes(-23)},
                    new Message { Text = "Sounds like a plan. \uD83D\uDE0E", IsIncoming = false, MessageDateTime = DateTime.Now.AddMinutes(-23)},

                    new Message { Text = "\uD83D\uDE48 \uD83D\uDE49 \uD83D\uDE49", IsIncoming = false, MessageDateTime = DateTime.Now.AddMinutes(-23)},

            });
        }

    }
}

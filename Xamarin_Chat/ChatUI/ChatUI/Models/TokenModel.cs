﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatUI.Models
{
    public class TokenModel
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}

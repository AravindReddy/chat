﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatUI.Models
{
    public class NewChannel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
        public string ChannelId { get; set; }
    }
}

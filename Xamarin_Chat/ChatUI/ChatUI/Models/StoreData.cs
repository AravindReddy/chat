﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatUI.Models
{
    public class ChannelUser
    {
        public string UniqueIdentifier { get; set; }
        public string UserName { get; set; }
    }

    public class ChannelAdmin
    {
        public string UniqueIdentifier { get; set; }
        public string UserName { get; set; }
    }

    public class ChannelRes
    {
        public int __v { get; set; }
        public string ChannelType { get; set; }
        public DateTime ChannelCreatedAt { get; set; }
        public string _id { get; set; }
        public List<ChannelUser> ChannelUsers { get; set; }
        public List<ChannelAdmin> ChannelAdmin { get; set; }
    }

    public class Sender
    {
        public string UniqueIdentifier { get; set; }
        public string UserName { get; set; }
    }

    public class ChannelAdmin2
    {
        public string UniqueIdentifier { get; set; }
        public string UserName { get; set; }
    }

    public class ChannelUser2
    {
        public string UniqueIdentifier { get; set; }
        public string UserName { get; set; }
    }

    public class MessageDoc
    {
        public int __v { get; set; }
        public DateTime MessageCreatedAt { get; set; }
        public string MessageText { get; set; }
        public string ChannelId { get; set; }
        public Sender Sender { get; set; }
        public string _id { get; set; }
        public List<object> DeliveredTo { get; set; }
        public List<object> ReadBy { get; set; }
        public List<ChannelAdmin2> ChannelAdmin { get; set; }
        public List<ChannelUser2> ChannelUsers { get; set; }
    }

    public class StoreData
    {
        public ChannelRes channelRes { get; set; }
        public MessageDoc messageDoc { get; set; }
    }
}